import unittest
from eval import *
from StringIO import StringIO

class my_test(unittest.TestCase):
    
    def test_add(self):
        self.assertEqual(eval(parse("(+ 1 2)")),3)
        self.assertEqual(eval(parse("(+ 1 2 3 4)")),10)  
        
    def test_multiply(self):
        self.assertEqual(eval(parse("(* 1 2 3 4)")),24)
        self.assertEqual(eval(parse("(* 1 2)")),2)

    def test_sub(self):
        self.assertEqual(eval(parse("(- 30 20)")),10)
        self.assertEqual(eval(parse("(- 30 20 10)")),0)

    def test_div(self):
        self.assertEqual(eval(parse("(/ 10 2)")),5)
        self.assertEqual(eval(parse("(/ 10 2 5)")),1)

    def invalid_expression(self,s):
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            #assert helper(s)==3
            with self.assertRaises(SystemExit) as cm:
                helper(s)
            output = out.getvalue().strip()
            assert output == 'Invalid Program'
        finally:
            sys.stdout = saved_stdout

    def test_invalid_expressions(self):
        a = [["()"],["(+ 1)"],["(+ 1)"],["(/ 10 0)"],["A"],["(+ (* 1 2) + (* 3 4))"],["(+ 1) 2)"]]
        for i in a:
            self.invalid_expression(i)

    def test_bind(self):
        s = ["(bind length 10)"]
        self.assertEqual(helper(s),10)

    def test_multiple_statements(self):
        s = ["(bind a 10)","(bind b a)","(bind a 11)","(+ a b)"]
        a = ["(bind length (+ 1 2))","(+ 1 2 3 4)","(bind breadth length)","(bind length 10)","(bind area (* length breadth))"]
        b = ["(+ 1 (* 2 3) (* 4 2))"]
        self.assertEqual(helper(s),21)
        self.assertEqual(helper(a),30)
        self.assertEqual(helper(b),15)

unittest.main()

